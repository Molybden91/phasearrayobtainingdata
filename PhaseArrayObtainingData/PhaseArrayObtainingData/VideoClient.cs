﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhaseArrayObtainingData
{
    enum CommandDlc {
        CameraBlockMotionDlc = 0x02,
        CameraBlockSetBrightDlc = 0x04,
    }
    enum CameraCommandsCode {
        CheckCameraModule       = 0x0101,
        TurnOnRightLight        = 0x0503,
        TurnOnLeftLight         = 0x0403,
        TurnOffLeftLight        = 0x0306,
        TurnOffRightLight       = 0x0307,
        TurnLaserTrue           = 0x0501,
        TurnLaserFalse          = 0x0502,
        LaserStatusRequest      = 0x0503,
        SetLeftLightBright      = 0x040308,
        GetLeftLightBright      = 0x02030a,
        GetRightLightBright     = 0x02030b,
        GetGasIndicatorDigital  = 0x020402,
        GetGasIndicatorAnalog   = 0x020401,
    }
    public struct CanFrame {
        public uint id;
        public uint dlc;
        public uint[] reserved;
        public uint[] data;

        CanFrame(uint dlc = 0 , uint id = 0) {
            data = new uint[8];
            reserved = new uint[3];
            this.dlc = dlc;
            this.id = id;
        }
    }
    public class VideoClient
    {
        uint Cameraid;
        CanFrame DataFrame;
        VideoClient(uint Cameraid) {
            this.Cameraid = Cameraid;
            DataFrame = new CanFrame();
        }
        bool cmd_Camera(uint commandCode, CommandDlc commandDlc)
        {
            DataFrame.id = Cameraid;
            DataFrame.dlc = (uint)commandDlc;
            for (int i = 0; i < 3; i++) {
                DataFrame.data[i] = (commandCode >> (8 * i)) & 0xFF;
            }
            return true;
        }
    }

}
