﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PhaseArrayObtainingData
{
    public struct ControlData {
        public ushort magic; // Magic const, can be: 0x8402,0x8401,0x8484
        public ushort phase; // M8402_Phase_xx WARNING: This is TRANSMITTER NUMBER from combobox of C - program.
        public ushort gain; // gain
        public ushort tgv;  // VRCH coefficient
        public ushort avg;  // accumulation number
        public ushort z_hp; // half period of sounder
        public ushort z_w; // the width of sounder impulse
        public ushort z_del; // sounder delay
        public ushort rec_del; // vector receiving delay
        public ushort shift; // shift between chanels
        public ushort z_count; // the number of sounder half periods
        public ushort rec_length; // vector lenght
        public ushort loop_del; // the interval between sounding
        public ushort mcu_del; // additional delay between sounding
    }

    public static class DefaultTestData{
        public static byte[] GetBytesFromControlData(ControlData data)
        {
            int size = Marshal.SizeOf(data);
            byte[] outputArray = new byte[size];
            IntPtr pointer = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(data, pointer, true);
            Marshal.Copy(pointer, outputArray, 0, size);
            Marshal.FreeHGlobal(pointer);
            return outputArray;
        }
        public static ControlData Default0 = new ControlData() {
	        magic      = 0x8401,
	        phase      = 2,
	        gain       = 930,
	        tgv        = 0, 
	        avg        = 16, 
	        z_hp       = 833,
	        z_w        = 788,
	        z_del      = 20479,
	        rec_del    = 4095, 
	        shift      = 92,
	        z_count    = 1, 
	        rec_length = 65535, 
	        loop_del   = 65535, 
	        mcu_del    = 3, 
        };
        public static ControlData Default1;
    }
}
