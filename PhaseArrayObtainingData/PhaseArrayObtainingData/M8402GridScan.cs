﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhaseArrayObtainingData
{
    public class M8402GridScan : GridScan
    {
        #region constants
        private const string M8402IPAddress = "192.168.1.140";
        private const int M8402Port = 3000;
        private const int StandartReplyDataLength = 4;
        private const int NumberOfVectors = 8;
        #endregion

        #region protected fields
        ArrM8402 arrayM8402;
        #endregion

        #region Constructors
        M8402GridScan(ScanningSettings scanningSettings, LatticeSettings latticeSettings) : base(scanningSettings, latticeSettings) {
            arrayM8402 = new ArrM8402();
        }
        #endregion

        #region protected methods
        protected bool StartGrid(int stepNum) {
            bool result = false;
            arrayM8402.SetAddress(M8402IPAddress, M8402Port);
            STGeneratorReceiverParams stParameters = new STGeneratorReceiverParams();
            arrayM8402.updateArrayConfig(stParameters);
            int ActiveChannel = (scanningStep - 1) % arrayM8402.ElementsNumber;
            result = arrayM8402.startZonder(ActiveChannel);
            return result;
        }
        protected bool GetGridData(int ScanningStep) {
            bool result = false;

            byte [] VectorsData = new byte[StandartReplyDataLength];
            result = arrayM8402.receiveZonderReply(VectorsData);
            int NumberOfBytesToReceive = BitConverter.ToInt32(VectorsData,0);
            if (result) {
                result = arrayM8402.receiveVectorsData(NumberOfBytesToReceive);

                int TotalVectorSize = arrayM8402.VectorLength * arrayM8402.ElementsNumber;
                int ReceivedVectorSize = TotalVectorSize / NumberOfVectors;
            }
            return result;
        }
        #endregion
    }
}
