﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhaseArrayObtainingData
{
    enum ScanningMotion {Horizontal, Vertical};
    public class ScanningSettings
    {
        #region Constructors
        ScanningSettings() {
            gasIndicatorType = false;
            isFullMatrix = false;

        }
        #endregion

        #region public methods

        #endregion

        #region private fields
        ScanningMotion scanMotion;
        int step;
        int velocity;
        bool slipEnabled;
        int latticeStep;
        int tubeDiameter;
        int tubeLength;
        int wallThickness;
        bool isFullMatrix; // Full-matrix scanning (each element is probed separately)
        bool gasIndicatorType;// 0 - digital, 1 - analog
        int continuousMoveVelocity;
        int fixAngle;
        int latticePartialPosition;
        #endregion

    }
}
