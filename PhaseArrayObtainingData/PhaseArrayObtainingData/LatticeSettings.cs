﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhaseArrayObtainingData
{
    enum EArrayLocation { Left, Right };
    enum EArrayConstruct { en2x8 = 28/*старый вариант АР*/, en3x5 = 35 /*Новый вариант АР*/ };
    public class LatticeSettings
    {
        #region constants
        private byte [] M8402IPAddress = {192,168,1,140};
        private const int M8402Port = 3000;
        #endregion

        #region public fields
        public Dictionary<string, int> Settings;
        #endregion

        #region private fields
        private STGeneratorReceiverParams stGeneratorReceiverParams; //Parameters of receiving tract/ generator signal
        private EArrayLocation receiverAr; //Receiving phase array
        private EArrayLocation senderAr; //Sender phase array
        private int senderChannel; // The element which generate signal
        private int testType; // Testing type
        private EArrayConstruct eArrayConstruct; 
        //Trailer Indicators
        private bool leftTop;
        private bool leftBottom;
        private bool rightTop;
        private bool rightBottom;
        private byte[] antArrIPAddr;
        private int antArrPort;
        #endregion

        #region Constructors 
        LatticeSettings()
        {
            receiverAr = EArrayLocation.Left;
            senderAr = EArrayLocation.Left;
            senderChannel = 1;
            testType = 1;
            leftTop = true;
            leftBottom = true;
            rightTop = true;
            rightBottom = true;
            eArrayConstruct = EArrayConstruct.en2x8;
            antArrIPAddr = M8402IPAddress;
            antArrPort = M8402Port;

            updateLatticeSettings();
            stGeneratorReceiverParams = new STGeneratorReceiverParams();
            if (Settings.Count == 0)
            {
                Settings.Add("AnalogueAmplification", stGeneratorReceiverParams.amplification);
                Settings.Add("DigitalAmplification", stGeneratorReceiverParams.digitalAmplification);
                Settings.Add("StorageNum", stGeneratorReceiverParams.storageNum);
                Settings.Add("WorkFreq", stGeneratorReceiverParams.workFreq);
                Settings.Add("HalfPeriodNum", stGeneratorReceiverParams.halfPeriodNum);
                Settings.Add("AdditionalDelay", stGeneratorReceiverParams.additionalDelay);
                Settings.Add("RandomDelay", stGeneratorReceiverParams.randomDelay);
                Settings.Add("Quantification", stGeneratorReceiverParams.quantification);
                Settings.Add("TestType", testType);
                Settings.Add("ArrayConstruct", (int)eArrayConstruct);

                Settings.Add("IPAddr", BitConverter.ToInt32(M8402IPAddress,0));
                Settings.Add("IPPort", antArrPort);
            }
            else {
                Settings.TryGetValue("AnalogueAmplification", out stGeneratorReceiverParams.amplification);
                Settings.TryGetValue("DigitalAmplification", out stGeneratorReceiverParams.digitalAmplification);
                Settings.TryGetValue("StorageNum", out stGeneratorReceiverParams.storageNum);
                Settings.TryGetValue("WorkFreq", out stGeneratorReceiverParams.workFreq);
                Settings.TryGetValue("HalfPeriodNum", out stGeneratorReceiverParams.halfPeriodNum);
                Settings.TryGetValue("AdditionalDelay", out stGeneratorReceiverParams.additionalDelay);
                Settings.TryGetValue("RandomDelay", out stGeneratorReceiverParams.randomDelay);
                Settings.TryGetValue("Quantification", out stGeneratorReceiverParams.quantification);
                Settings.TryGetValue("TestType", out testType);
            }
        }
        #endregion

        #region public methods

        #endregion

        #region private methods
        private void updateLatticeSettings() {
            this.Settings = new Dictionary<string, int>();
        }
        #endregion
    }
}
