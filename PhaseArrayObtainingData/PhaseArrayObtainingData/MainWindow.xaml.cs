﻿using OxyPlot;
using OxyPlot.Axes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PhaseArrayObtainingData
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public PlotModel ModelForGeneralPicture { get; set; }
        public OxyPlot.Series.HeatMapSeries GeneralHeatMap = new OxyPlot.Series.HeatMapSeries();
        public OxyPlot.Axes.LinearColorAxis HeatAxis;
        public double[,] DataForHeatMap { get; private set; }
        #region private variables
        private int PointsForOneGraph = 4096;
        private List<List<short[]>> AllDataObtainedFromFolder = new List<List<short[]>>();
        private List<short[]> DataFromOneScan = new List<short[]>();
        private bool isRunning = false;
        private int NumberOfScanning = 0;
        #endregion
        public MainWindow()
        {
            ModelForGeneralPicture = new PlotModel();
            HeatAxis = new OxyPlot.Axes.LinearColorAxis
            {
                Key = "z",
                Position = AxisPosition.Top,
                LowColor = OxyColors.White,
                HighColor = OxyColors.Red,
                Palette = new OxyPalette(OxyColors.LightGray, OxyColors.Gray, OxyColors.Black)
            };
            ModelForGeneralPicture.Axes.Add(HeatAxis);
            ModelForGeneralPicture.Series.Add(GeneralHeatMap);
            InitializeComponent();
        }

        #region Application Window Events
        // Action that will be performed after application window closure
        private void OnWindowCloseEvent(object sender, CancelEventArgs e)
        {
            //Environment.Exit(Environment.ExitCode);
        }
        #endregion

        #region Button Events
        private void ButtonSingleConnectionTest_Click(object sender, EventArgs e) {
            EthernetDevice ethernetDevice = new EthernetDevice("192.168.1.140", 3000);
            ScanningAction(ethernetDevice);
        }

        private void ButtonFullScan_Click(object sender, EventArgs e) {
            EthernetDevice ethernetDevice = new EthernetDevice("192.168.1.140", 3000);
            ControlData settingsToSend = DefaultTestData.Default0;
            byte[] AllData = new byte[65536 * 8];
            for (int i = 0; i < 8; i++) {
                settingsToSend.phase = (ushort)i;
                ethernetDevice.ConnectDevice();
                byte[] forSend = DefaultTestData.GetBytesFromControlData(settingsToSend);
                ethernetDevice.SendData(forSend, forSend.Length);
                byte[] obtainedData = new byte[65536];
                byte[] checkingData = new byte[4];
                ethernetDevice.ReceiveData(checkingData, 4);
                Thread.Sleep(200);
                ethernetDevice.ReceiveData(obtainedData);
                for (int j = 0; j < obtainedData.Length; j++) {
                    AllData[((i * 65536) + j)] = obtainedData[j];
                }
                ethernetDevice.DisconnectDevice();
            }
            DrawResult();
            PrepareData(AllData);
        }

        private void ButtonStart_Click(object sender, EventArgs e) {
            isRunning = true;
        }

        private void ButtonStop_Click(object sendere, EventArgs e) {
            isRunning = false;
        }
        #endregion

        #region private methods
        private void PrepareData(byte[] obtainedData) {
            short[] vector = new short[4096];
            int j = 0;
            for (int i = 0; i < obtainedData.Length; i=i+2) {
                byte[] tmp = {obtainedData[i],obtainedData[i+1]};
                vector[j] = BitConverter.ToInt16(tmp, 0);
                j++;
                if (j == 4096) {
                    j = 0;
                    DataFromOneScan.Add(vector);
                    vector = new short[4096];
                }
            }
            this.DataForHeatMap = new double[PointsForOneGraph,DataFromOneScan.Count];
            for (int i = 0; i < DataFromOneScan.Count; i++) {
                short[] oneVector = DataFromOneScan[i];
                for (int k = 0; k < oneVector.Length; k++) {
                    DataForHeatMap[k, i] = oneVector[k] ;
                }
            }
        }
        private void ScanningAction(EthernetDevice ethernetDevice) {
            this.DataFromOneScan = new List<short[]>();
            ethernetDevice.ConnectDevice();
            ControlData settingsToSend = DefaultTestData.Default0;
            byte[] forSend = DefaultTestData.GetBytesFromControlData(settingsToSend);
            ethernetDevice.SendData(forSend, forSend.Length);
            byte[] obtainedData = new byte[65536];
            byte[] checkingData = new byte[4];
            byte[] AllData = new byte[65536 * 8];
            ethernetDevice.ReceiveData(checkingData, 4);
            Thread.Sleep(200);
            ethernetDevice.ReceiveData(obtainedData);
            PrepareData(obtainedData);
            DrawResult();
            ethernetDevice.DisconnectDevice();
        }
        private void DrawResult() {

            GeneralHeatMap.Data = DataForHeatMap;
            GeneralHeatMap.X0 = 0;
            GeneralHeatMap.Y0 = 0;
            GeneralHeatMap.X1 = PointsForOneGraph;
            GeneralHeatMap.Y1 = DataFromOneScan.Count;
            PlotForInputFile.Model = ModelForGeneralPicture;
            for (int i = 0; i < DataFromOneScan.Count; i++) {
                SourceDistribution(i);
            }
            PlotForInputFile.InvalidatePlot(true);
        }
        private void SourceDistribution(int seriesNumber)
        {
            Graph currentGraph = (Graph)V3_5Temp.FindName("V3_5_"+Convert.ToString(seriesNumber+1));
            short[] vector = DataFromOneScan[seriesNumber];
            List<DataPoint> points = new List<DataPoint>();
            for (int j = 0; j < vector.Length; j++) {
                points.Add(new DataPoint(j,vector[j]));
            }
            currentGraph.LineSeries.ItemsSource = points;
        }
            #endregion
            #region Static Methods
            //Obtain local IP (not localhost like 127.0.0.1)
            private static string GetLocalIpAddress()
        {
            string localIP = "";
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                }
            }
            return localIP;
        }
        #endregion
    }
}
