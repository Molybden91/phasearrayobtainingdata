﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhaseArrayObtainingData
{
    /// <summary>
    /// Represenataion of tract parameters
    /// </summary>
    public struct STGeneratorReceiverParams
    {
        public int amplification; // Analog amplification (DB)
        public int digitalAmplification; // Digital amplification
        public int storageNum; // The number of accumulation
        public int workFreq; // Working frequency
        public int halfPeriodNum; // The number of half periods
        public int additionalDelay; // Additional delay
        public int randomDelay; // random delay
        public int quantification; // 0 - 1 MHz ; 1 - 4 MHz;	// quantification frequency
        public int vectorLength; // 4096
        public int vectorMaxAmplitude; //Maximum possible signal module
    }
    public class DefaultSTGeneratorParams {
        const int ctiDefVectorSize = 4096;
        const int ctiDefSignalAmplitudeRange = 20480;

        public static STGeneratorReceiverParams STGeneratorReceiverParamsDefault = new STGeneratorReceiverParams() {
            amplification        = 35,// Аналоговое усиление
            digitalAmplification = 0, 	// Цифровое усиление
		    storageNum           = 16, 	// Колчиество накоплений
		    workFreq             = 55, // Рабочая частота
		    halfPeriodNum        = 2,	// Число полупериодов
		    additionalDelay      = 15,	// Дополнительная задержка
		    randomDelay          = 5,// Случайная задержка
		    quantification       = 0, // 0 - 1 MHz ; 1 - 4 MHz;	// частота квантования
		    vectorLength         = ctiDefVectorSize, // 4096
		    vectorMaxAmplitude   = ctiDefSignalAmplitudeRange,
        };
    }
}
