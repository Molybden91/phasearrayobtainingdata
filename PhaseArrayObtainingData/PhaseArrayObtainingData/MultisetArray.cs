﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhaseArrayObtainingData
{
    /// <summary>
    /// Description of phase array structure. Phase array with several elements sets.
    /// </summary>
    public abstract class MultisetArray
    {
        #region protected fields
        protected int m_NElements;
        protected int m_NSets;
        #endregion

        #region Constructors
        MultisetArray(int NElements, int NSets)
        {
            this.m_NElements = NElements;
            this.m_NSets = NSets;
        }
        #endregion

        #region public methods
        public uint getElementMask(int ElementNum) {
            uint Mask = 0;
            int Num = toInternal(ElementNum);
            if (Num < m_NElements) {
                Mask = maskForElement(Num);
            }
            return Mask;
        }
        public int getSetMask(int SetNum) {
            int Mask = 0;
            return Mask;
        }
        public int internalNum(int ExternalNum) {
            int result = toInternal(ExternalNum);
            return result;
        }
        public int externalNum(int InternalNum) {
            int result = InternalNum;
            for (result = 0; result < m_NElements; result++) {
                if (toInternal(result) == InternalNum) break;
            }
            return result;
        }

        #endregion

        #region protected methods
        protected int toInternal(int Num) {
            return Num;
        }
        protected uint maskForElement(int ElementNum) {
            uint Mask = 0;
            return Mask;
        }
        protected abstract uint MaskForSet(int iSetNum);
        #endregion
    }
}
