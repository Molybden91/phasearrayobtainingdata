﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PhaseArrayObtainingData
{
    public class ArrM8402 : EthernetDevice
    {
        #region constants
        private const int ChannelsNumberConst = 8;
        private const int DefaultVectorLengthConst = 4096;
        private const double FixedVectorLengthConst = 4096;
        private const int InternalVectorDecimationRatioConst = 10;
        private const float InternalQuantFreqMHzConst = 10;
        private const int DecimationConst = 20;
        private const int DecimationForSingleQuantification = 5;
        private const int VeryLargeTimeoutInReceivingLarge = 10000000;
        private const string M8402IPAddress = "192.168.1.140";
        private const int M8402Port = 3000;
        #endregion

        #region protected fields
        public int ElementsNumber { get; set; }
        public int VectorLength { get; set; }
        protected double ReceiveVectorSize { get; set; }
        protected ControlData conf_data;
        #endregion

        #region private fields
        private byte [] DataBuffer;
        #endregion

        #region Constructors
        public ArrM8402() : base(M8402IPAddress, M8402Port){
            VectorLength = DefaultVectorLengthConst;
            ReceiveVectorSize = FixedVectorLengthConst;
            DataBuffer = new byte[0];
            ElementsNumber = ChannelsNumberConst;
            toDefaultConfig();
        }
        public ArrM8402(string ipAddress, int ipPort) : base(ipAddress, ipPort) {
            VectorLength = DefaultVectorLengthConst;
            ReceiveVectorSize = FixedVectorLengthConst;
            DataBuffer = new byte[0];
            ElementsNumber = ChannelsNumberConst;
            toDefaultConfig();
        }
        #endregion

        #region public methods
        /// <summary>
        /// Connect with phase array and send settings to it. And close socket and connection if we are unable to send data to device.
        /// </summary>
        /// <param name="ChannelNumber">The number of active channel</param>
        /// <returns></returns>
        public bool startZonder(int ChannelNumber) {
            bool result = false;
            int RequestSize = signalRequest(DataBuffer, ChannelNumber);

            if (ConnectDevice())
            {
                result = SendData(DataBuffer, RequestSize);
                if (!result)
                {
                    socket.Shutdown(System.Net.Sockets.SocketShutdown.Both);
                    socket.Close();
                }
            }else {
                throw new Exception("Couldn't connect Array device with IPAddr: "+ ipAddress+"   Port: "+ipPort);
            }
            return result;
        }
        /// <summary>
        /// Check if we are ready to receive the target data. This methods can make us sure that we are able to obtain data. We must receive 4 bytes, 
        /// which represent the lenght of valuable data we can obtain.
        /// </summary>
        /// <param name="VectorData">The byte array for received 4 bytes</param>
        /// <returns></returns>
        public bool receiveZonderReply(byte [] VectorData) {
            bool result = false;
            int Received = ReceiveData(VectorData, 4, 10000000);
            if (Received == 4)
            {
                result = true;
            }
            else {
                throw new Exception("Unable to receive target data");
            }
            return result;
        }
        /// <summary>
        /// Method for receiving all target data from phase array.  
        /// (I remaded the method ReceiveLargeData method of Ethernet Device class for obtaing the number of received bytes instead of bool value)
        /// </summary>
        /// <param name="VectorDataSize">Total size of data that we want to obtain </param>
        /// <returns></returns>
        public bool receiveVectorsData(int VectorDataSize) {
            bool result = false;
            int Received = 0;
            int VectorPosition = 0;
            int ReceivedVectorSize = VectorDataSize / ElementsNumber;
            byte[] tmpBuffer = new byte[ReceivedVectorSize];
            for (int i = 0; i < ElementsNumber; i++) {
                Received = ReceiveLargeData(tmpBuffer, ReceivedVectorSize);
                for (int j = 0; j < ReceivedVectorSize;j++) {
                    DataBuffer[j + VectorPosition] = tmpBuffer[j];
                }
                VectorPosition += Received;
            }
            if (VectorDataSize == DataBuffer.Length)
            {
                result = true;
            }
            return result;
        }
        /// It Seems like not necessary in our sharp code. Our we should develop similar functional
        //public void receivedVectors(short pVectors, int TotalVectorSize) { }
        /// It Seems like not necessary in our sharp code. 
        //public bool receivedVector(int ChannelNum, int pVector, int VectorSize) {}
        /// <summary>
        /// Phase array settings actualisation 
        /// </summary>
        /// <param name="sTGeneratorReceiverParams"> The tract parameters</param>
        public void updateArrayConfig(STGeneratorReceiverParams sTGeneratorReceiverParams) {
            ControlData ArrayConfig = this.conf_data;
            ArrayConfig.gain = (ushort)(sTGeneratorReceiverParams.amplification * (2185/ 46.95));
            ArrayConfig.avg = (ushort)sTGeneratorReceiverParams.storageNum;
            ArrayConfig.z_count = (ushort)(sTGeneratorReceiverParams.halfPeriodNum - 1);

            if (ArrayConfig.z_count < 1) ArrayConfig.z_count = 1;
            if (ArrayConfig.z_count > 15) ArrayConfig.z_count = 15;

            ArrayConfig.z_hp = (ushort)(10000000 / sTGeneratorReceiverParams.workFreq * 2000);
            ArrayConfig.z_w = (ushort)((10000000/sTGeneratorReceiverParams.workFreq*2000) - 50);

            if (sTGeneratorReceiverParams.quantification == 1) {
                ArrayConfig.tgv = (ushort)DecimationForSingleQuantification;
                ArrayConfig.rec_length = (ushort)(sTGeneratorReceiverParams.vectorLength * 2.5 - 1);
            } else {
                ArrayConfig.tgv = (ushort)DecimationConst;
                ArrayConfig.rec_length = (ushort)(sTGeneratorReceiverParams.vectorLength*InternalVectorDecimationRatioConst - 1);
            }
            ArrayConfig.mcu_del = (ushort)sTGeneratorReceiverParams.additionalDelay;
            conf_data = ArrayConfig;
        }
        /// <summary>
        /// Obtaining internal (formal) number from external (working number) 
        /// </summary>
        /// <param name="ExternalNum">working number</param>
        /// <returns></returns>
        int internalNum(int ExternalNum) {
            int result = toInternal(ExternalNum);
            return result;
        }
        /// <summary>
        /// Obtaining external (working number) from internal (formal number)  
        /// </summary>
        /// <param name="InternalNum">formal number</param>
        /// <returns></returns>
        int externalNum(int InternalNum) {
            int result = InternalNum;
            for (result = 0; result < ElementsNumber; result++) {
                if (toInternal(result) == InternalNum) {
                    break;
                }
            }
            return result;
        }
        #endregion

        #region protected methods
        /// <summary>
        /// Obtaining of the internal number from external 
        /// </summary>
        /// <param name="iNum">Internal number</param>
        /// <returns></returns>
        protected int toInternal(int iNum) {
            return ElementsNumber - 1 - iNum;
        }
        /// <summary>
        /// Set config to default using predefined parameters.
        /// </summary>
        protected void toDefaultConfig() {
            conf_data = DefaultTestData.Default0;
        }
        /// <summary>
        /// It seems like we don't need to specify the size of a buffer, because of byte array usage instead of the char pointer (as int the origin program was)
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="ActiveChannel">The number of active channel</param>
        /// <returns>The length of configuration structure in bytes</returns>
        protected int signalRequest(byte [] buffer, int ActiveChannel) {
            int configlenght = DefaultTestData.GetBytesFromControlData(conf_data).Length;
            if (buffer != null && buffer.Length >= configlenght) {
                conf_data.phase = (ushort)internalNum(ActiveChannel);
                return configlenght;
            }
            return 0;
        }
        #endregion

    }
}
