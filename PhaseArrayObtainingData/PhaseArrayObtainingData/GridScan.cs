﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhaseArrayObtainingData
{
    public enum scanState
    {
        WaitingForStep,
        MakingStep,
        GetGridDataFlag,
        StepIsDone,
        RunningGrid,
        DataObtained,
        ScanningStopped,
        ScanningPaused,
        GettingVideoFrame,
        GettingAllDataFromBuffer,
        Error
    };
    public enum WheelPosition {
        LeftForward = 0,
        LeftBack = 1,
        RightForward = 2,
        RightBack = 3
    };
    public class GridScan
    {
        #region constants
        #endregion

        #region protected fields
        protected bool scanningPaused;
        protected bool getPathLength;
        protected bool stepRF, stepRB, stepLF, stepLB;
        protected int fileCount;
        protected int g_step;
        protected int scanningStep;
        protected int pathRF, pathRB, pathLF, pathLB;
        protected scanState state;
        protected double angle1;
        protected double angle2;
        protected ScanningSettings scanningSettings;
        protected LatticeSettings latticeSettings;
        protected string scanData;
        protected double m_dYaw, m_pPitch, m_dRoll;
        #endregion

        #region Constructors
        public GridScan(ScanningSettings scanningSettings, LatticeSettings latticeSettings) {
            this.latticeSettings = latticeSettings;
            this.scanningSettings = scanningSettings;
            m_dYaw = 0.0f;
            m_dRoll = 0.0f;
            m_pPitch = 0.0f;
        }
        #endregion

        #region public methods
        //virtual void scanStep(QString workingAddress, CANbus::CanQueryResult* canQuery, const QString &currentTubeElem) = 0;
        //virtual bool continueScanning(QString workingAddress, CANbus::CanQueryResult* canQuery, const QString &currentTubeElem) = 0;
        //virtual void initScanner(QString fileNameTemplate, int fileCounter) = 0;
        public void setWheelPath(int path, WheelPosition wheel) { }
        public void resetErrorCounter() {
        }
        public void changeState(scanState val) { state = val; }
        public scanState returnState() { return state; }
        //public void setSocket(SOCKET socket) { sck = socket; }
        //public void setSaveFileTemplate(QString str) { saveFileTemplate = str; }
        public void setFileCounter(int fileCounter) { fileCount = fileCounter; }

        public double getFirstGridAngle() { return angle1; }
        public double getSecondGridAngle() { return angle2; }
        /// <summary>
        /// Defining the number of scanning cycles per one scanner position
        /// </summary>
        /// <returns>The number of scanning cycles</returns>
        public int NZondersPerStep() {
            return 1;
        }
        public void setCurAngles(double dYaw, double dPitch, double dRoll)
        {
            m_dYaw = dYaw;
            m_pPitch = dPitch;
            m_dRoll = dRoll;
        }
        #endregion

        #region protected methods
        protected int nVectorsInPack() {
            return 1;
        }
        protected bool saveDataToFile(int iVectorsDataSize) {
            return true;
        }
        protected int packSizeBytes() {
            return 1;
        }
        protected int scanZonderRequest(byte [] pBuffer, int iBuffSize, int stepNum, LatticeSettings settings, int scanningStep) {
            return 1;
        }
        protected bool startGrid(int stepNum) {
            return true;
        }
        protected bool getGridData(int iScanningStep) {
            return true;
        }
        //protected void sortVectors(BYTE* pData, int vectorSizeBytes, char* pStoreMem, int iScanPos);
        protected bool makeStep() {
            return true;
        }
        protected bool isStepDone() {
            return true;
        }

        protected int vectorSize() {
            return 1;
        }
        protected int vectorSizeBytes() {
            return 1;
        }
        #endregion

    }
}
